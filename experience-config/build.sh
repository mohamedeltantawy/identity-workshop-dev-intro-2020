#!/usr/bin/env bash

zip experience-configuration.zip experience-configuration.json && \
  zip provisioning-package.zip experience-configuration.zip manifest.json && \
  java -jar ./bin/cx6-import-tool-cli-6.1.3.jar \
    --import provisioning-package.zip \
    --auth-url=http://localhost:8180 \
    --target-ctx=http://localhost:8080/gateway/api/provisioning \
    --username=admin \
    --password=admin \
    --identity-client-id=bb-tooling-client \
    --identity-realm=backbase \
    --identity-grant-type=password \
    --identity-scope=openid