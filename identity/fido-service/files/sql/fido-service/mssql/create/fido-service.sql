-- Changeset src/main/resources/db/changelog/db.changelog-1_5_0.yml::fido001::jimd
CREATE TABLE [application] ([id] [int] IDENTITY (1, 1) NOT NULL, [app_key] [nvarchar](128) NOT NULL, [app_id] [nvarchar](255) NOT NULL, CONSTRAINT [pk_application] PRIMARY KEY ([id]))
GO

ALTER TABLE [application] ADD CONSTRAINT [uq_application_app_key] UNIQUE ([app_key])
GO

ALTER TABLE [application] ADD CONSTRAINT [uq_application_app_id] UNIQUE ([app_id])
GO

CREATE TABLE [trusted_facet] ([id] [int] IDENTITY (1, 1) NOT NULL, [application_id] [int] NOT NULL, [trusted_facet_id] [nvarchar](128), CONSTRAINT [pk_trusted_facet] PRIMARY KEY ([id]))
GO

ALTER TABLE [trusted_facet] ADD CONSTRAINT [fk_trusted_facet_to_app] FOREIGN KEY ([application_id]) REFERENCES [application] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION
GO

CREATE TABLE [registration_request] ([id] [int] IDENTITY (1, 1) NOT NULL, [application_id] [int] NOT NULL, [challenge] [nvarchar](86), [device_id] [nvarchar](36) NOT NULL, [user_id] [nvarchar](36) NOT NULL, [username] [nvarchar](255) NOT NULL, [processed] [bit] NOT NULL, CONSTRAINT [pk_registration_request] PRIMARY KEY ([id]))
GO

ALTER TABLE [registration_request] ADD CONSTRAINT [fk_registration_request_to_app] FOREIGN KEY ([application_id]) REFERENCES [application] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION
GO

CREATE TABLE [registration] ([id] [int] IDENTITY (1, 1) NOT NULL, [application_id] [int] NOT NULL, [device_id] [nvarchar](36) NOT NULL, [user_id] [nvarchar](36) NOT NULL, [username] [nvarchar](255) NOT NULL, [aa_id] [nvarchar](128), [public_key_base_64_encoded] [nvarchar](512), [key_id] [nvarchar](128), [signature_counter] [bigint], [authenticator_version] [int], CONSTRAINT [pk_registration] PRIMARY KEY ([id]))
GO

ALTER TABLE [registration] ADD CONSTRAINT [fk_registration_to_app] FOREIGN KEY ([application_id]) REFERENCES [application] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION
GO

CREATE TABLE [authentication_request] ([id] [int] IDENTITY (1, 1) NOT NULL, [application_id] [int] NOT NULL, [challenge] [nvarchar](86), [user_id] [nvarchar](36) NOT NULL, [username] [nvarchar](255) NOT NULL, [processed] [bit] NOT NULL, CONSTRAINT [pk_authentication_request] PRIMARY KEY ([id]))
GO

ALTER TABLE [authentication_request] ADD CONSTRAINT [fk_authen_request_to_app] FOREIGN KEY ([application_id]) REFERENCES [application] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION
GO

