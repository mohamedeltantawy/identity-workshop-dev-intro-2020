-- Changeset src/main/resources/db/changelog/db.changelog-1_5_0.yml::device001::gavin
CREATE TABLE [device] ([device_id] [nvarchar](36) NOT NULL, [user_id] [nvarchar](36), [username] [nvarchar](255), [status] [nvarchar](30), [created_date_time] [bigint], [updated_date_time] [bigint], CONSTRAINT [pk_device] PRIMARY KEY ([device_id]))
GO

-- Changeset src/main/resources/db/changelog/db.changelog-1_5_1_1.yml::device002::gavin
-- Creates a new, nullable column for a users dbs userId
ALTER TABLE [device] ADD [dbs_user_id] [nvarchar](36)
GO

-- Changeset src/main/resources/db/changelog/db.changelog-1_5_2.yml::device003::corey
-- Creates an index for users dbs userId
CREATE NONCLUSTERED INDEX ix_dbs_user_id ON [device]([dbs_user_id])
GO

ALTER TABLE [device] ADD [friendly_name] [nvarchar](50)
GO

ALTER TABLE [device] ADD [vendor] [nvarchar](100)
GO

ALTER TABLE [device] ADD [model] [nvarchar](100)
GO

