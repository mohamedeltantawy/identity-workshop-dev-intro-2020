-- Changeset src/main/resources/db/changelog/db.changelog-1_5_1_1.yml::device002::gavin
-- Creates a new, nullable column for a users dbs userId
ALTER TABLE [device] ADD [dbs_user_id] [nvarchar](36)
GO

