SET DEFINE OFF;

-- Changeset src/main/resources/db/changelog/db.changelog-1_5_0.yml::device001::gavin
CREATE TABLE device (device_id VARCHAR2(36) NOT NULL, user_id VARCHAR2(36), username VARCHAR2(255), status VARCHAR2(30), created_date_time NUMBER(38, 0), updated_date_time NUMBER(38, 0), CONSTRAINT pk_device PRIMARY KEY (device_id));

-- Changeset src/main/resources/db/changelog/db.changelog-1_5_1_1.yml::device002::gavin
-- Creates a new, nullable column for a users dbs userId
ALTER TABLE device ADD dbs_user_id VARCHAR2(36);

-- Changeset src/main/resources/db/changelog/db.changelog-1_5_2.yml::device003::corey
-- Creates an index for users dbs userId
CREATE INDEX ix_dbs_user_id ON device(dbs_user_id);

ALTER TABLE device ADD friendly_name VARCHAR2(50);

ALTER TABLE device ADD vendor VARCHAR2(100);

ALTER TABLE device ADD model VARCHAR2(100);

