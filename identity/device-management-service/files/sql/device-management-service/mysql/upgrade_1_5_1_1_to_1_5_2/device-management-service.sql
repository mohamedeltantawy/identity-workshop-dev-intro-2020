--  Changeset src/main/resources/db/changelog/db.changelog-1_5_2.yml::device003::corey
--  Creates an index for users dbs userId
CREATE INDEX ix_dbs_user_id ON device(dbs_user_id);

ALTER TABLE device ADD friendly_name VARCHAR(50) NULL, ADD vendor VARCHAR(100) NULL, ADD model VARCHAR(100) NULL;

