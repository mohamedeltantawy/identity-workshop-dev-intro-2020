--  Changeset src/main/resources/db/changelog/db.changelog-1_5_0.yml::device001::gavin
CREATE TABLE device (device_id VARCHAR(36) NOT NULL, user_id VARCHAR(36) NULL, username VARCHAR(255) NULL, status VARCHAR(30) NULL, created_date_time BIGINT NULL, updated_date_time BIGINT NULL, CONSTRAINT pk_device PRIMARY KEY (device_id));

--  Changeset src/main/resources/db/changelog/db.changelog-1_5_1_1.yml::device002::gavin
--  Creates a new, nullable column for a users dbs userId
ALTER TABLE device ADD dbs_user_id VARCHAR(36) NULL;

--  Changeset src/main/resources/db/changelog/db.changelog-1_5_2.yml::device003::corey
--  Creates an index for users dbs userId
CREATE INDEX ix_dbs_user_id ON device(dbs_user_id);

ALTER TABLE device ADD friendly_name VARCHAR(50) NULL, ADD vendor VARCHAR(100) NULL, ADD model VARCHAR(100) NULL;

