#! /bin/bash

if [ -z "$LOGGING_LEVEL" ]; then
 export LOGGING_LEVEL='INFO';
fi

echo "Logging Level : $LOGGING_LEVEL"
exec java -cp "mysql-connector-java.jar:backbase-identity.jar" org.wildfly.swarm.bootstrap.Main \
-Smysql \
-s db-config/project-mysql.yml \
-s config/project.yml \
-Djava.net.preferIPv4Stack=true \
-Djboss.server.config.dir=/opt/bb-identity/users \
-Dthorntail.keycloak-server.themes.defaults.dir=/opt/bb-identity/themes \
-Dkeycloak.import=/opt/bb-identity/realm/backbase-realm.json \
-Dthorntail.backbase.identity.events.audit.url=$AUDIT_INTEGRATION_URI \
-Dswarm.logging=$LOGGING_LEVEL
