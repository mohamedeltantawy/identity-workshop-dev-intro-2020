#!/usr/bin/env bash


IDENTITY_URL="http://localhost:8180"
EDGE_HOST="localhost"

mvn package bb:provision  -Dportal.host="$EDGE_HOST" -Dportal.port=8080 -Dcontextpath=/gateway/api -Didentity.url="$IDENTITY_URL"

mvn bb:import-experience  -Dportal.host="$EDGE_HOST" -Dportal.port=8080 -Dcontextpath=/gateway/api -Didentity.url="$IDENTITY_URL"

mvn bb:import-packages -Dportal.host="$EDGE_HOST" -Dportal.port=8080 -Dcontextpath=/gateway/api -Didentity.url=$IDENTITY_URL

cd collection-ku8s-project
mvn package bb:provision  -Dportal.host="$EDGE_HOST" -Dportal.port=8080 -Dcontextpath=/gateway/api -Didentity.url="$IDENTITY_URL"

