#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

### Input user ###
read -r -p "Enter Ldap username: "  username
### Input password as hidden charactors ###
read -r -s -p "Enter Ldap password: "  password

# Add backbase-charts repo
helm repo add backbase-charts https://repo.backbase.com/backbase-charts --username "$username" --password "$password"
helm repo update
